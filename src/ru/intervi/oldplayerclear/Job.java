package ru.intervi.oldplayerclear;

import org.bukkit.OfflinePlayer;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.World;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.domains.DefaultDomain;

import ru.intervi.littleconfig.FileStringList;
import ru.intervi.littleconfig.utils.Utils;

import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.UUID;

public class Job extends Thread {
	private WorldGuardPlugin wg = null;
	private int days = 0;
	private boolean dat, ess, wgrld, hch, au;
	
	public Job(int d, boolean date, boolean essentials, boolean worldguard, boolean herochat, boolean authme) {
		days = d;
		dat = date; ess = essentials; wgrld = worldguard; hch = herochat; au = authme;
		Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
		if (plugin != null || (plugin instanceof WorldGuardPlugin)) wg = (WorldGuardPlugin) plugin;
	}
	
	@Override
	public void run() {
		ArrayList<OfflinePlayer> p = getPlayers();
		try {if (dat) remDat(p);} catch(Exception e) {e.printStackTrace();}
		try {if (ess) remEss(p);} catch(Exception e) {e.printStackTrace();}
		try {if (wgrld) remWg(p);} catch(Exception e) {e.printStackTrace();}
		try {if (hch) remHchat(p);} catch(Exception e) {e.printStackTrace();}
		try {if (au) remAuthme(p);} catch(Exception e) {e.printStackTrace();}
		try {saveList(p);} catch(Exception e) {e.printStackTrace();}
		System.out.println("чистка завершена");
	}
	
	private ArrayList<OfflinePlayer> getPlayers() {
		OfflinePlayer p[] = Bukkit.getOfflinePlayers();
		ArrayList<OfflinePlayer> result = new ArrayList<OfflinePlayer>();
		if (p == null) return result;
		long stamp = System.currentTimeMillis();
		for (int i = 0; i < p.length; i++) {
			if (p[i] == null) continue;
			double d = (double) ((double) stamp / 1000 / 60 / 60 / 24) - ((double) p[i].getLastPlayed() / 1000 / 60 / 60 / 24);
			if (d >= days) result.add(p[i]);
		}
		return result;
	}
	
	private void saveList(ArrayList<OfflinePlayer> p) {
		if (p == null) return;
		ArrayList<String> out = new ArrayList<String>();
		out.add("ник: [кол-во дней с последнего захода, uuid]");
		Iterator<OfflinePlayer> iter = p.iterator();
		long stamp = System.currentTimeMillis();
		while(iter.hasNext()) {
			OfflinePlayer player = iter.next();
			if (player == null) continue;
			double d = (double) ((double) stamp / 1000 / 60 / 60 / 24) - ((double) player.getLastPlayed() / 1000 / 60 / 60 / 24);
			String name = player.getName();
			String uuid = player.getUniqueId().toString();
			if (name == null || uuid == null) continue;
			out.add((name + ": [" + d + ", " + uuid + "]"));
		}
		if (out.isEmpty()) return;
		String path = Utils.getFolderPath(this.getClass()) + File.separator + "OldPlayerClear.yml";
		FileStringList fs = new FileStringList();
		fs.list = out;
		fs.write(path);
	}
	
	private void remDat(ArrayList<OfflinePlayer> p) {
		if (p == null) return;
		String path = Utils.getFolderPath(this.getClass());
		path = path.substring(0, path.lastIndexOf(File.separator));
		Properties prop = new Properties();
		try {
			prop.load(new FileReader((path + File.separator + "server.properties")));
		} catch(Exception e) {e.printStackTrace();}
		String w = prop.getProperty("level-name");
		if (w == null) return;
		path += File.separator + w;
		Iterator<OfflinePlayer> iter = p.iterator();
		while(iter.hasNext()) {
			OfflinePlayer player = iter.next();
			if (player == null) continue;
			String name = player.getName();
			String uuid = player.getUniqueId().toString();
			if (name == null || uuid == null) continue;
			File file = new File((path + File.separator + "playerdata" + File.separator + uuid + ".dat"));
			if (file.isFile()) file.delete();
			file = new File((path + File.separator + "stats" + File.separator + uuid + ".json"));
			if (file.isFile()) file.delete();
			file = new File((path + File.separator + "playerdata" + File.separator + name + ".dat"));
			if (file.isFile()) file.delete();
			file = new File((path + File.separator + "stats" + File.separator + name + ".dat"));
			if (file.isFile()) file.delete();
		}
	}
	
	private void remEss(ArrayList<OfflinePlayer> p) {
		if (p == null) return;
		String path = Utils.getFolderPath(this.getClass()) + File.separator + "Essentials" + File.separator + "userdata";
		if (!(new File(path).isDirectory())) return;
		Iterator<OfflinePlayer> iter = p.iterator();
		while(iter.hasNext()) {
			OfflinePlayer player = iter.next();
			if (player == null) continue;
			String name = player.getName();
			String uuid = player.getUniqueId().toString();
			if (name == null || uuid == null) continue;
			File file = new File((path + File.separator + uuid + ".yml"));
			if (file.isFile()) file.delete();
			file = new File((path + File.separator + name.toLowerCase() + ".yml"));
			if (file.isFile()) file.delete();
		}
	}
	
	private void remHchat(ArrayList<OfflinePlayer> p) {
		if (p == null) return;
		String path = Utils.getFolderPath(this.getClass()) + File.separator + "Herochat" + File.separator + "chatters";
		if (!(new File(path).isDirectory())) return;
		Iterator<OfflinePlayer> iter = p.iterator();
		while(iter.hasNext()) {
			OfflinePlayer player = iter.next();
			if (player == null) continue;
			String name = player.getName();
			if (name == null) continue;
			File file = new File((path + File.separator + name.substring(0, 1) + File.separator + name + ".yml"));
			if (file.isFile()) file.delete();
		}
	}
	
	private void remWg(ArrayList<OfflinePlayer> p) {
		if (p == null || wg == null) return;
		int hash[] = new int[p.size()];
		for(int i = 0; i < hash.length; i++) {
			OfflinePlayer player = p.get(i);
			if (player == null) continue;
			UUID uuid = player.getUniqueId();
			if (uuid == null) continue;
			hash[i] = uuid.hashCode();
		}
		List<World> w = Bukkit.getWorlds();
		Iterator<World> iter = w.iterator();
		while(iter.hasNext()) {
			World world = iter.next();
			if (world == null) continue;
			RegionManager manager = wg.getRegionManager(world);
			Map<String, ProtectedRegion> map = manager.getRegions();
			Set<Entry<String, ProtectedRegion>> set = map.entrySet();
			Iterator<Entry<String, ProtectedRegion>> setiter = set.iterator();
			while(setiter.hasNext()) {
				Entry<String, ProtectedRegion> entry = setiter.next();
				if (entry == null) continue;
				ProtectedRegion region = entry.getValue();
				DefaultDomain owndomain = region.getOwners();
				Set<UUID> ownsetuuid = owndomain.getUniqueIds();
				int check[] = new int[ownsetuuid.size()];
				Iterator<UUID> owniteruuid = ownsetuuid.iterator();
				int n = 0;
				while(owniteruuid.hasNext()) {
					UUID uuid = owniteruuid.next();
					if (uuid == null) {n++; continue;}
					check[n] = uuid.hashCode();
					n++;
				}
				for (int i = 0; i < check.length; i++) {
					if (check[i] == 0) continue;
					for (int k = 0; k < hash.length; k++) {
						if (hash[k] == 0) continue;
						if (hash[k] == check[i]) {
							OfflinePlayer player = p.get(k);
							if (player == null) continue;
							owndomain.removePlayer(player.getUniqueId());
						}
					}
				}
				DefaultDomain memdomain = region.getMembers();
				Set<UUID> memsetuuid = owndomain.getUniqueIds();
				check = new int[memsetuuid.size()];
				Iterator<UUID> memiteruuid = memsetuuid.iterator();
				n = 0;
				while(memiteruuid.hasNext()) {
					UUID uuid = memiteruuid.next();
					if (uuid == null) {n++; continue;}
					check[n] = uuid.hashCode();
					n++;
				}
				for (int i = 0; i < check.length; i++) {
					if (check[i] == 0) continue;
					for (int k = 0; k < hash.length; k++) {
						if (hash[k] == 0) continue;
						if (hash[k] == check[i]) {
							OfflinePlayer player = p.get(k);
							if (player == null) continue;
							memdomain.removePlayer(player.getUniqueId());
						}
					}
				}
				if (owndomain.size() == 0 & memdomain.size() == 0) manager.removeRegion(entry.getKey());
			}
			try {
				manager.save();
			} catch(Exception e) {e.printStackTrace(); break;}
		}
	}
	
	private void remAuthme(ArrayList<OfflinePlayer> p) {
		if (p == null) return;
		String path = Utils.getFolderPath(this.getClass()) + File.separator + "AuthMe" + File.separator + "auths.db";
		File base = new File(path);
		if (!base.isFile()) return;
		int hash[] = new int[p.size()];
		for(int i = 0; i < hash.length; i++) {
			OfflinePlayer player = p.get(i);
			if (player == null) continue;
			String name = player.getName();
			if (name == null) continue;
			hash[i] = name.toLowerCase().hashCode();
		}
		FileStringList fs = new FileStringList(path);
		for (int i = 0; i < fs.list.size(); i++) {
			String line = fs.list.get(i);
			if (line == null || line.indexOf(':') == -1) continue;
			int check = line.split(":")[0].toLowerCase().hashCode();
			for (int k = 0; k < hash.length; k++) {
				if (check == hash[k]) {
					fs.list.remove(i);
					break;
				}
			}
		}
		File back = new File((Utils.getFolderPath(this.getClass()) + File.separator + "AuthMe" + File.separator + "auths.db.old"));
		base.renameTo(back);
		fs.write(path);
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "authme reload");
	}
}