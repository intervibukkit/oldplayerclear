package ru.intervi.oldplayerclear;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	private Job Job;
	
	@Override
	public void onDisable() {
		if (Job != null) Job.destroy();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command com, String label, String[] args) {
		if (Job != null && Job.isAlive()) {
			sender.sendMessage("чистка уже запущена");
			return true;
		}
		boolean result = false;
		if (sender.isOp()) {
			String send[] = {
					"===========================================",
					" Данный плагин удаляет файлы игроков из:",
					" - Стандартные",
					" - Essentials",
					" - HeroChat",
					" Чистит базы:",
					" - WorldGuard (удаляет из региона и удаляет пустые регионы)",
					" - AuthMe (только БД на текстовом файле)",
					" Использование:",
					" /opc [кол-во дней с последнего захода] d e w h a",
					" d - dat, e - essentials, w - worldguard, h - herochat",
					" a - authme",
					" Пример:",
					" /opc 30 d h",
					" Можно не указывать параметры, кроме кол-ва дней.",
					" По окончании чистки в директории с плагинами",
					" появится файл OldPlayerClear.yml формата:",
					" ник: [кол-во дней с последнего захода, uuid]",
					"==========================================="
			};
			if (com.getName().equalsIgnoreCase("opc") && args == null | args.length == 0) {
				sender.sendMessage(send);
			}
			if (com.getName().equalsIgnoreCase("opc") && args != null && args.length >= 1) {
				int d = 0;
				try {
					d = Integer.parseInt(args[0]);
				} catch(Exception e) {
					sender.sendMessage(send);
					return true;
				}
				boolean dat = false, ess = false, wgrld = false, hch = false, au = false;
				if (args.length > 1) {
					for (int i = 2; i < args.length; i++) {
						char c[] = args[i].toCharArray();
						if (c.length > 1) {
							sender.sendMessage(send);
							return true;
						}
						switch (c[0]) {
						case 'd':
							dat = true;
							break;
						case 'e':
							ess = true;
							break;
						case 'w':
							wgrld = true;
							break;
						case 'h':
							hch = true;
							break;
						case 'a':
							au = true;
							break;
						default:
							sender.sendMessage(send);
							return true;
						}
					}
				}
				Job Job = new Job(d, dat, ess, wgrld, hch, au);
				Job.start();
				sender.sendMessage("чистка запущена");
			}
		} else sender.sendMessage("нет прав");
		return result;
	}
}